
start-netsims:
	for I in $$(seq $(NUM_DEVICES)); do \
		docker run -td --name $(CNT_PREFIX)-dev$${I} --network-alias dev$${I} --hostname dev$${I} --cpuset-cpus=$(NETSIM_CPUS) $(DOCKER_ARGS) --label com.cisco.nso.testenv.type=netsim $(NETSIM_IMAGE) & \
	done

# TODO: also filter on our PNS label
stop-netsims:
	docker ps -aq --filter label=com.cisco.nso.testenv.type=netsim | $(XARGS) docker rm -vf

NED_NAME=$(shell docker exec -it $(CNT_PREFIX)-$* ls /var/opt/ncs/packages/)
tmp/add-devices/%:
	@echo "-- Add device to NSO"
	@echo "   Get the package-meta-data.xml file from the compiled NED (we grab it from the netsim build)"
	mkdir -p tmp/add-devices
	docker cp $(CNT_PREFIX)-$*:/var/opt/ncs/packages/$(NED_NAME)/package-meta-data.xml tmp/$*-package-meta-data.xml
	@echo "   Fill in the device-type in add-device.xml by extracting the relevant part from the package-meta-data of the NED"
	xmlstarlet sel -t -c "//*[_:ned-id] | //_:ned/_:netconf | //_:ned/_:cli | //_:ned/_:snmp | //_:ned/_:generic" tmp/$*-package-meta-data.xml | xmlstarlet edit --subnode '_:*[not(_:ned-id)]' --type elem -n ned-id -v dummy | xmlstarlet sel -R -t -c '/' -c "document('props/add-device.xml')" | xmlstarlet edit -O -N x=http://tail-f.com/ns/ncs-packages -N y=http://tail-f.com/ns/ncs -d "/x:xsl-select/*[x:ned-id]/*[not(self::x:ned-id)]" -m "/x:xsl-select/*[x:ned-id]" "/x:xsl-select/y:devices/y:device/y:device-type" | tail -n +2 | sed '$$d' | cut -c3- | xmlstarlet edit -O --update '/_:devices/_:device/_:name' --value '$*' --update '/_:devices/_:device/_:address' --value '$*' > $@

add-devices.xml: $(addprefix tmp/add-devices/,$(DEVICES))
	head -n 1 tmp/dev1-add-device.xml > $@
	for F in $(addprefix tmp/add-devices/,$(DEVICES)); do \
		tail -n +2 $${F} | head -n -1 >> $@; \
	done
	tail -n 1 tmp/dev1-add-device.xml >> $@
