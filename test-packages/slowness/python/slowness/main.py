# -*- mode: python; python-indent: 4 -*-
from contextlib import contextmanager
import random
import time

import ncs
from ncs.application import Service


@contextmanager
def ptrace_service(service, message, verbosity=ncs.VERBOSITY_NORMAL):
    t = ncs.maagic.get_trans(service)
    progress = t.report_service_progress_start(verbosity, message, service._path, '')
    try:
        yield
    finally:
        t.report_service_progress_stop(progress)


class ServiceCallbacks(Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        with ptrace_service(service, 'sleep configured time'):
            time.sleep(float(service.create_slowness))

            rand_max = float(service.random_create_slowness) * 1000
            if rand_max > 0:
                with ptrace_service(service, 'nested random sleep'):
                    time.sleep(random.randrange(0, int(rand_max)) / 1000)


class Main(ncs.application.Application):
    def setup(self):
        self.register_service('slowness-servicepoint', ServiceCallbacks)
