#!/bin/bash

NUM_DEVICES=$1

for I in $(seq ${NUM_DEVICES}); do
    echo "Starting netsim ${I}"
    docker run -td --name ${CNT_PREFIX}-netsim${I} --network-alias netsim${I} 
    sleep 10

done
