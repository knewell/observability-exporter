# -*- mode: python; python-indent: 4 -*-
import ncs
from ncs.application import Service


class AclyServiceCallbacks(Service):

    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        for i in range(service.scale_factor):
            acl = service.computed.acls.create(f"ACL{i}")
            for j in range(service.scale_factor):
                _ = acl.terms.create(f"TERM{j}")

        vars = ncs.template.Variables()
        template = ncs.template.Template(service)
        template.apply('acl', vars)


class Main(ncs.application.Application):
    def setup(self):
        self.register_service('acly-servicepoint', AclyServiceCallbacks)
